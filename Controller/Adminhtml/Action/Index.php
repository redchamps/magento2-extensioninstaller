<?php
namespace RedChamps\ExtensionInstaller\Controller\Adminhtml\Action;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage);
        $resultPage->getConfig()->getTitle()->prepend(__('Extension Installer'));
        return $resultPage;
    }

    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('RedChamps_ExtensionInstaller::extensioninstaller')
            ->addBreadcrumb(__('RedChamps'), __('RedChamps'))
            ->addBreadcrumb(__('Extension Installer'), __('Extension Installer'));
        return $resultPage;
    }
}
