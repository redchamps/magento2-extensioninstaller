<?php
namespace RedChamps\ExtensionInstaller\Controller\Adminhtml\Action;

class Install extends \Magento\Backend\App\Action
{

    protected $_directoryList;

    protected $_installer;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    )
    {
        $this->_directoryList = $directoryList;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            if(isset($_FILES['package']['name'])) {
                $uploader = new \Magento\Framework\File\Uploader('package');
                $uploader->setAllowedExtensions(array('zip'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $tmpPath  = $this->_directoryList->getPath('var').'/RedChamps/tmp/';
                $response = $uploader->save($tmpPath, $_FILES['package']['name']);
                $file     = $response['file'];
                $filepath = $tmpPath . $file;
                $zip = new \ZipArchive();
                $x = $zip->open($filepath);
                if ($x === true) {
                    $composerFile = $zip->getFromName('composer.json');
                    if($composerFile) {
                        $composerFileContent = json_decode($composerFile, true);
                        if(isset($composerFileContent['autoload'])) {
                            $modulePath = array_filter(explode('\\', key($composerFileContent['autoload']['psr-4'])));
                            if(count($modulePath) == 2) {
                                $targetPath = $this->_directoryList->getPath('app').'/code/'.$modulePath[0].'/'.$modulePath[1].'/';
                                $zip->extractTo($targetPath); // change this to the correct site path
                                $zip->close();
                                $this->messageManager->addSuccess('Extension has been installed successfully. Please go ahead and activate it from System > Web Setup Wizard > Component Manager.');
                            } else {
                                $this->messageManager->addError('composer.json file of package file doesn\'t contain valid entry under "psr-4" section.');
                            }
                        } else {
                            $this->messageManager->addError('composer.json file of package file doesn\'t contain autoload section.');
                        }
                    } else {
                        $this->messageManager->addError('Package file is invalid as doesn\'t contain a composer.json file.');
                    }
                    unlink($filepath);
                } else {
                    $this->messageManager->addError('Some error occurred during extension installation.');

                }
            } else {
                $this->messageManager->addError('Extension package is not selected. Please retry.');
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('extensioninstaller/action/index');
    }
}