# Magento 2 Extension Installer

You may be aware of that Magento 2 has no native functionality to directly install extension package via admin like Magento 1 have Magento connect manager. So we have developed this Magento 2 extension to allow installation of Magento 2 zip extension package via admin. The installation process is just simple as choosing your extension package from your system and clicking install button. Please note that this extension will only install extension package, it will not activate the extension. you have to activate extension manually by following below steps:

1. Go to admin path System > Web Setup Wizard > Component Manager.
2. Find installed extension in component grid and select 'Enable' from 'Action' column and follow further steps asked by Magento 2 system.

##Installation Instructions

This extension can be manually installed by uploading file system via FTP or can be installed by executing below commands via SSH in your Magento 2 root directory.

1. composer require redchamps/extensioninstaller
2. php bin/magento setup:upgrade

Visit our store for more extensions [RedChamps.com](https://redchamps.com)
